package com.uklon.sherbakov.model.repo.remote

import arrow.core.Either
import com.uklon.sherbakov.extension.webCall
import com.uklon.sherbakov.model.dataBase.entity.Comment
import com.uklon.sherbakov.model.dataBase.entity.Post
import com.uklon.sherbakov.model.dataBase.entity.User
import com.uklon.sherbakov.model.web.ServerApi
import com.uklon.sherbakov.model.web.WebErrors
import io.reactivex.Single
import io.reactivex.functions.BiFunction

/**
 * Created by antony on 08.05.18.
 */
class WebDataProvider(private val web : ServerApi)  {

    /**
     * GET all posts
     * return list of posts OR WebError
     */
    fun getAllPosts(action : (Either<WebErrors, List<Post>>) -> Unit) {
        web.getAllPosts().webCall()
                .subscribe({ action(Either.Right(it)) }, { action(WebErrors.handleError(it)) })
    }

    /**
     * GET all comments by selected post, and filter post`s user
     * return Pair of user and list of comments OR WebError
     */
    fun getCommentsAndAuthorInfo(post : Post, action : (Either<WebErrors, Pair<User?, List<Comment>>>) -> Unit) {

        val biFunction = BiFunction<List<User>, List<Comment>, Pair<User?, List<Comment>>> { userList, commentList ->
            userList.find { it.id == post.userId } to commentList }

        Single.zip(web.getUsers(), web.getComments(post.id), biFunction)
                .webCall()
                .subscribe( { action(Either.Right(it)) }, { action(WebErrors.handleError(it)) })
    }

    /**
     * GET all comments by selected post
     * return list of comments OR WebError
     */
    fun getComments(postId : Long, action : (Either<WebErrors, List<Comment>>) -> Unit) {
        web.getComments(postId)
                .webCall()
                .subscribe({ action(Either.Right(it)) }, { action(WebErrors.handleError(it)) })
    }
}
