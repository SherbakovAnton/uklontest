package com.uklon.sherbakov.model.dataBase.entity

/**
 * Created by antony on 08.05.18.
 */

data class User (
        val id          : Long,
        val name        : String,
        val userName    : String,
        val phone       : String,
        val website     : String,
        val address     : Address,
        val company     : Company
)

data class Address (
        val street      : String,
        val suite       : String,
        val city        : String,
        val zipcode     : String
)

data class Company (
        val name        : String,
        val catchPhrase : String,
        val bs          : String
)