package com.uklon.sherbakov.model.web

import arrow.core.Eval
import arrow.syntax.function.andThen
import arrow.syntax.function.forwardCompose
import com.google.gson.GsonBuilder
import com.uklon.sherbakov.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by antony on 08.05.18.
 */

class WebService {

    private val timeOut = 60L
    private val period = TimeUnit.SECONDS

    val retrofitInstance = Eval.Later<ServerApi> {
           (::initHttpLogging forwardCompose
            ::buildBaseHttpClient forwardCompose
            ::buildRetrofit ).invoke()
    }

    private fun buildBaseHttpClient (
            loggingInterceptor : HttpLoggingInterceptor? = null ) =
            OkHttpClient()
                    .newBuilder()
                    .apply {  loggingInterceptor?.let { addInterceptor(it) } }
                    .connectTimeout(timeOut, period)
                    .readTimeout(timeOut, period)
                    .build()

    private fun buildRetrofit(okHttpClient : OkHttpClient) =
            Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                    .baseUrl(BuildConfig.baseUrl)
                    .client(okHttpClient)
                    .build()
                    .create(ServerApi::class.java)

    private fun initHttpLogging() =
            HttpLoggingInterceptor()
                    .apply { level = HttpLoggingInterceptor.Level.BODY }

}