package com.uklon.sherbakov.model.dataBase.entity

/**
 * Created by antony on 08.05.18.
 */

data class Comment (
        val id      : Long,
        val postId  : Long,
        val email   : String,
        val body    : String
)
