package com.uklon.sherbakov.model.koin

import com.uklon.sherbakov.model.repo.remote.WebDataProvider
import com.uklon.sherbakov.model.web.WebService
import com.uklon.sherbakov.viewModel.InternetConnectionViewModel
import com.uklon.sherbakov.viewModel.PostViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

/**
 * Created by antony on 08.05.18.
 */

val uklonModule = applicationContext {

    /******************* VIEW MODEL ********************/

    viewModel   { InternetConnectionViewModel(get())    }
    viewModel   { PostViewModel(get())                       }

    /********************** BEAN ***********************/

    bean        { WebService().retrofitInstance.value   }
    bean        { WebDataProvider(get())                }



}