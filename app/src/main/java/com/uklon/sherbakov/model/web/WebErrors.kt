package com.uklon.sherbakov.model.web

import android.support.annotation.IdRes
import arrow.core.Either
import com.uklon.sherbakov.R

/**
 * Created by antony on 09.05.18.
 */

enum class WebErrors constructor(@IdRes val idStringRes : Int) {
    errorLoadingComment(R.string.error_loading_comment),
    errorServer(R.string.error_server);

    /**
     * не хочу заморачиваться, но этот метод создается для управления ошибками,
     * чтоб на них, в последующем, соответственно реагировать
     * Да, тут я захардкодил...
     */
    companion object {
        fun handleError(throwable : Throwable) : Either<WebErrors, Nothing> {
            return Either.Left(errorServer)
        }
    }

}