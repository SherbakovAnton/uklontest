package com.uklon.sherbakov.model.dataBase.entity

/**
 * Created by antony on 08.05.18.
 */
data class Post (
        val id      : Long,
        val userId  : Long,
        val title   : String,
        val body    : String
)