package com.uklon.sherbakov.model.web

import com.uklon.sherbakov.model.dataBase.entity.Comment
import com.uklon.sherbakov.model.dataBase.entity.Post
import com.uklon.sherbakov.model.dataBase.entity.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by antony on 08.05.18.
 */

interface ServerApi {

    @GET("/posts")
    fun getAllPosts() : Single<List<Post>>

    @GET("/comments")
    fun getComments(@Query("postId") postId : Long) : Single<List<Comment>>

    @GET("/users")
    fun getUsers() : Single<List<User>>

}