package com.uklon.sherbakov.view.androidView

import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout

/**
 * Created by antony on 09.05.18.
 */

@BindingAdapter("swipe_action")
fun SwipeRefreshLayout.swipe_action_(action : () -> Unit) {
    setOnRefreshListener { action() }
}