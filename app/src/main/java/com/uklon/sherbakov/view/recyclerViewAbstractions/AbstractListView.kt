package com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout

/**
 * Created by antony on 05.04.18.
 */

open abstract class AbstractListView<Item, ItemView : View>(context: Context?, attrs: AttributeSet?)
        : RelativeLayout(context, attrs),
          ListOperator<Item> {

    abstract val adapter : RecyclerViewAdapterBase<Item, ItemView>

    override fun addItem(stubs: Item)           = adapter.addItem(stubs)

    override fun addItems(stubs: List<Item>)    = adapter.addItems(stubs)

    override fun setItems(stubs: List<Item>)    = adapter.setItems(stubs)

    override fun removeItem(stubs: Item)        = adapter.removeItem(stubs)

    override fun removeAll()                    = adapter.removeAll()

}