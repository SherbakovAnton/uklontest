package com.uklon.sherbakov.view.androidView

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.BindingAdapter
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.uklon.sherbakov.R
import com.uklon.sherbakov.extension.initialize
import com.uklon.sherbakov.model.dataBase.entity.Comment
import com.uklon.sherbakov.model.dataBase.entity.User
import kotlinx.android.synthetic.main.view_user_panel.view.*

/**
 * Created by antony on 09.05.18.
 */
class UserPanel(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    @SuppressLint("SetTextI18n")
    internal fun bind(user: User?) {
        user?.let {
            userName.text = "${it.name}, ${it.userName}"
            userAddress.text = "${it.address.city}, ${it.address.street}"
            userCompany.text = it.company.name
            userPhone.text = it.phone
            userWeb.text = it.website
        }
    }

    init {
        initialize(R.layout.view_user_panel)
    }
}

@BindingAdapter("bindUser")
fun UserPanel.bindUser_(user: User?) { this.bind(user) }
