package com.uklon.sherbakov.view.androidView

import android.content.Context
import android.databinding.BindingAdapter
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.AbstractProgressRecyclerView
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.RecyclerViewAdapterBase
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.ViewWrapper
import com.uklon.sherbakov.R
import com.uklon.sherbakov.extension.initialize
import com.uklon.sherbakov.model.dataBase.entity.Post
import kotlinx.android.synthetic.main.cell_view_post.view.*

/**
 * Created by antony on 08.05.18.
 */

class PostRecyclerView(context: Context, attrs: AttributeSet?) : AbstractProgressRecyclerView<Post, PostCellView>(context, attrs) {

    override val adapter by lazy { PostAdapter(context).apply { initAdapter() } }

    init { recyclerView.adapter = adapter }

    var onItemClick : ((Post) -> Unit)? = null
        set(value) { adapter.onItemClick = value }

}

class PostCellView(context: Context) : RelativeLayout(context) {

    internal fun bindKpi(post: Post) {
        post.run {
            postTitle.text = title
            postBody.text = body
        }
    }

    init { initialize(R.layout.cell_view_post) }
}

/** ADAPTER */
class PostAdapter(private val context: Context) : RecyclerViewAdapterBase<Post, PostCellView>() {

    var onItemClick : ((Post) -> Unit)? = null

    override fun onBindViewHolder(holder: ViewWrapper<PostCellView>?, position: Int) {
        items[position].also { kpi ->
            val view = holder?.view
            view?.bindKpi(kpi)

            view?.setOnClickListener {
                onItemClick?.invoke(kpi)
            }
        }
    }

    override fun onCreateItemView(parent: ViewGroup, viewType: Int) = PostCellView(context)
}

@BindingAdapter("onPostClicked")
fun PostRecyclerView.onPostClicked_(onItemClick : (Post) -> Unit) {
    this.onItemClick = onItemClick
}