package com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.uklon.sherbakov.extension.plus

/**
 * Created by antony on 05.04.18.
 */

abstract class RecyclerViewAdapterBase<T, V : View> : RecyclerView.Adapter<ViewWrapper<V>>(), ListOperator<T> {

    val items : MutableList<T> by lazy { ArrayList<T>() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewWrapper<V> = ViewWrapper(onCreateItemView(parent, viewType))

    protected abstract fun onCreateItemView(parent: ViewGroup, viewType: Int): V

    override fun getItemCount(): Int = items.size

    fun initAdapter(items: List<T>? = null) = items?.let { this.items.addAll(it) }

    override fun addItem(stub: T)           = items.add(stub) + notifyDataSetChanged()

    override fun addItems(stubs: List<T>)   = items.addAll(stubs) + notifyDataSetChanged()

    override fun setItems(stubs: List<T>)   {  items.clear() + items.addAll(stubs) + notifyDataSetChanged() }

    override fun removeItem(stub: T)        = items.remove(stub) + notifyDataSetChanged()

    override fun removeAll()                = items.clear() + notifyDataSetChanged()

}