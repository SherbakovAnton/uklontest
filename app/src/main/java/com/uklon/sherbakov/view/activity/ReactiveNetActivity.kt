package com.uklon.sherbakov.view.activity

import android.annotation.SuppressLint
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Receiver
import org.androidannotations.annotations.SystemService

/**
 * Created by antony on 08.05.18.
 */
@EActivity abstract class ReactiveNetActivity : AppCompatActivity() {

    @SystemService lateinit var connection : ConnectivityManager

    open val noInternetAction = {  }
    open val isInternetAction = {  }

    @SuppressLint("MissingPermission")
    @Receiver(actions = ["android.net.conn.CONNECTIVITY_CHANGE"])
    fun onChangeInternetConnection() {
        connection.activeNetworkInfo?.let {
            if (it.isConnectedOrConnecting) { isInternetAction() ; return }
        }
        noInternetAction()
    }

    @SuppressLint("MissingPermission")
    fun doIfConnected(action : () -> Unit) {
        connection
                .activeNetworkInfo
                ?.isConnectedOrConnecting
                ?.let {if (it) action() }
    }

}