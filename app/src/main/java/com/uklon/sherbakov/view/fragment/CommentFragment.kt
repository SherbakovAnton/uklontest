package com.uklon.sherbakov.view.fragment

import android.databinding.ViewDataBinding
import android.support.v4.app.Fragment
import arrow.core.Either
import arrow.syntax.function.pipe
import com.uklon.sherbakov.R
import com.uklon.sherbakov.databinding.FragmentCommentBinding
import com.uklon.sherbakov.extension.observe
import com.uklon.sherbakov.extension.toast
import com.uklon.sherbakov.model.dataBase.entity.Comment
import com.uklon.sherbakov.model.web.WebErrors
import com.uklon.sherbakov.viewModel.PostViewModel
import kotlinx.android.synthetic.main.fragment_comment.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.BindingObject
import org.androidannotations.annotations.DataBound
import org.androidannotations.annotations.EFragment
import org.koin.android.architecture.ext.sharedViewModel

/**
 * Created by antony on 09.05.18.
 */

@DataBound @EFragment(R.layout.fragment_comment) open class CommentFragment : Fragment() {

    private val postViewModel by sharedViewModel<PostViewModel>()

    @BindingObject fun injectBinding(binding : ViewDataBinding) {
        (binding as FragmentCommentBinding).postViewModel = postViewModel
    }

    @AfterViews fun onInit() {
        this to postViewModel.updateCommentsLiveData observe ::onUpdateComments
    }

    private fun onUpdateComments(either : Either<WebErrors, List<Comment>>) {
        either.map { it pipe commentListView::setItems }
                .mapLeft { it.idStringRes pipe ::toast }
        commentListView.cancelRefreshing()
    }
}