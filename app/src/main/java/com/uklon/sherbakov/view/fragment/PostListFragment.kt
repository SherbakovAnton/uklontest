package com.uklon.sherbakov.view.fragment

import android.databinding.ViewDataBinding
import android.support.v4.app.Fragment
import arrow.core.Either
import arrow.syntax.function.pipe
import com.uklon.sherbakov.R
import com.uklon.sherbakov.databinding.FragmentPostListBinding
import com.uklon.sherbakov.extension.addFragment
import com.uklon.sherbakov.extension.observe
import com.uklon.sherbakov.extension.toast
import com.uklon.sherbakov.model.dataBase.entity.Comment
import com.uklon.sherbakov.model.dataBase.entity.Post
import com.uklon.sherbakov.model.dataBase.entity.User
import com.uklon.sherbakov.model.web.WebErrors
import com.uklon.sherbakov.viewModel.InternetConnectionViewModel
import com.uklon.sherbakov.viewModel.PostViewModel
import kotlinx.android.synthetic.main.fragment_post_list.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.BindingObject
import org.androidannotations.annotations.DataBound
import org.androidannotations.annotations.EFragment
import org.koin.android.architecture.ext.sharedViewModel

/**
 * Created by antony on 08.05.18.
 */

@DataBound
@EFragment(R.layout.fragment_post_list)
open class PostListFragment : Fragment() {

    private val internetConnectionViewModel     by sharedViewModel<InternetConnectionViewModel>()
    private val postViewModel                   by sharedViewModel<PostViewModel>()

    @BindingObject fun injectBinding(binding : ViewDataBinding) {
        (binding as FragmentPostListBinding).postViewModel = postViewModel
        binding.internetConnectionViewModel = internetConnectionViewModel
    }

    @AfterViews
    fun onInit() {
        internetConnectionViewModel.postList.subscribe(  ::receivePosts)
        this to postViewModel.commentsLiveData   observe ::receiveComments
    }

    private fun receivePosts(posts : Either<WebErrors, List<Post>>) {
        posts.map { postRecyclerView.setItems(it) }
                .mapLeft { it.idStringRes pipe ::toast }
        postRecyclerView.cancelRefreshing()
    }

    private fun receiveComments(comments : Either<WebErrors, Pair<User?, List<Comment>>>) {
        comments.map { addFragment(fragmentContainer.id, CommentFragment_.builder().build()) }
                .mapLeft { it.idStringRes pipe ::toast }
    }
}

