package com.uklon.sherbakov.view.androidView

import android.annotation.SuppressLint
import android.content.Context
import android.databinding.BindingAdapter
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.AbstractProgressRecyclerView
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.RecyclerViewAdapterBase
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.ViewWrapper
import com.uklon.sherbakov.R
import com.uklon.sherbakov.extension.initialize
import com.uklon.sherbakov.model.dataBase.entity.Comment
import kotlinx.android.synthetic.main.cell_view_comment.view.*

/**
 * Created by antony on 09.05.18.
 */

class CommentListRecyclerView(context: Context, attrs: AttributeSet?)
    : AbstractProgressRecyclerView<Comment, CommentCellView>(context, attrs) {

    override val adapter by lazy { CommentAdapter(context).apply { initAdapter() } }
    init {
        recyclerView.adapter = adapter
    }
}

class CommentCellView(context: Context) : RelativeLayout(context) {

    @SuppressLint("SetTextI18n")
    internal fun bind(comment : Comment) {
       comment.run {
           userEmail.text = "${context.getString(R.string.user_email)} ${this.email}"
           commentBody.text = "${context.getString(R.string.user_comment)} ${this.body}"
        }
    }

    init { initialize(R.layout.cell_view_comment) }
}

/** ADAPTER */
class CommentAdapter(private val context: Context) : RecyclerViewAdapterBase<Comment, CommentCellView>() {

    override fun onBindViewHolder(holder: ViewWrapper<CommentCellView>?, position: Int) {
        items[position].run { holder?.view?.bind(this) }
    }

    override fun onCreateItemView(parent: ViewGroup, viewType: Int) = CommentCellView(context)
}

@BindingAdapter("bindComments")
fun CommentListRecyclerView.bindComments_(comments: List<Comment>) { this.setItems(comments) }

@BindingAdapter("onSwipeAction")
fun CommentListRecyclerView.onSwipeAction_(action : () -> Unit) = onSwipeAction(action)
