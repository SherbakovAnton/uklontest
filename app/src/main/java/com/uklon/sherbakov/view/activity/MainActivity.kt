package com.uklon.sherbakov.view.activity

import arrow.core.Either
import arrow.syntax.function.pipe
import com.uklon.sherbakov.R
import com.uklon.sherbakov.extension.setFragment
import com.uklon.sherbakov.extension.toast
import com.uklon.sherbakov.model.dataBase.entity.Post
import com.uklon.sherbakov.model.web.WebErrors
import com.uklon.sherbakov.view.fragment.PostListFragment_
import com.uklon.sherbakov.viewModel.InternetConnectionViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.koin.android.architecture.ext.viewModel

@EActivity(R.layout.activity_main) open class MainActivity : ReactiveNetActivity() {

    private val internetConnectionViewModel : InternetConnectionViewModel by viewModel()

    override val isInternetAction = { internetConnectionViewModel.getAll() }
    override val noInternetAction = { this toast R.string.no_internet }

    val  disposable : CompositeDisposable = CompositeDisposable()

    /****************************************** METHODS *******************************************/

    @AfterViews fun onInit() {
        internetConnectionViewModel::getAll                             pipe ::doIfConnected
        internetConnectionViewModel.postList.subscribe(::onReceivePost) pipe disposable::add
    }

    private fun onReceivePost(either : Either<WebErrors, List<Post>>) {
        either.map { onGetPosts(it) }.mapLeft { it.idStringRes pipe ::toast }
        disposable.dispose()
    }

    private fun onGetPosts(list : List<Post>) {
        setFragment(rootMainActivity, PostListFragment_.builder().build())
    }
}
