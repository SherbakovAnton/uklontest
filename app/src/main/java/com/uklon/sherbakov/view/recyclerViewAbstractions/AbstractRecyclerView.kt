package com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View
import com.uklon.sherbakov.R
import com.uklon.sherbakov.extension.initialize
import kotlinx.android.synthetic.main.wraper_view.view.*

/**
 * Created by antony on 10.04.18.
 */
abstract class AbstractProgressRecyclerView<Item, ItemView : View>(context: Context?, attrs: AttributeSet?)
                                                        : AbstractListView<Item, ItemView>(context, attrs) {

    val recyclerView by lazy { findViewById<RecyclerView>(R.id.abstractRecyclerView)
                                .apply { layoutManager = LinearLayoutManager(context) } }

    fun onSwipeAction(action : () -> Unit) {
        abstractSwipeRefreshLayout.isEnabled = true
        abstractSwipeRefreshLayout.setOnRefreshListener {
            action()
            abstractSwipeRefreshLayout.isRefreshing = true
        }
    }

    fun cancelRefreshing() {
        abstractSwipeRefreshLayout.isRefreshing = false
    }

    init {
        initialize(R.layout.wraper_view)
        abstractSwipeRefreshLayout.isEnabled = false
    }
}

