package com.uklon.sherbakov.viewModel

import android.arch.lifecycle.ViewModel
import arrow.core.Either
import arrow.syntax.function.pipe
import com.uklon.sherbakov.model.dataBase.entity.Post
import com.uklon.sherbakov.model.repo.remote.WebDataProvider
import com.uklon.sherbakov.model.web.WebErrors
import io.reactivex.subjects.BehaviorSubject

/**
 * Created by antony on 08.05.18.
 */

class InternetConnectionViewModel ( private val remoteProvider : WebDataProvider) : ViewModel() {

    val postList = BehaviorSubject.create<Either<WebErrors, List<Post>>>()

    private val getPostList : (Either<WebErrors, List<Post>>) -> Unit = { postList.onNext(it) }

    fun getAll() = ::getPostList.get() pipe remoteProvider::getAllPosts

    val getAllAction = { getAll() }

}