package com.uklon.sherbakov.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import arrow.core.Either
import com.uklon.sherbakov.model.dataBase.entity.Comment
import com.uklon.sherbakov.model.dataBase.entity.Post
import com.uklon.sherbakov.model.dataBase.entity.User
import com.uklon.sherbakov.model.repo.remote.WebDataProvider
import com.uklon.sherbakov.model.web.WebErrors

/**
 * Created by antony on 09.05.18.
 */

open class PostViewModel (private val remoteProvider : WebDataProvider) : ViewModel() {

    val commentsLiveData        = MutableLiveData<Either<WebErrors, Pair<User?, List<Comment>>>>()
    val updateCommentsLiveData  = MutableLiveData<Either<WebErrors, List<Comment>>>()

    val onPostClick : (Post) -> Unit = {
        remoteProvider.getCommentsAndAuthorInfo(it, commentsLiveData::postValue)
    }

    val updateComments : () -> Unit = {
        getPostId()?.let { remoteProvider.getComments(it, updateCommentsLiveData::postValue) }
    }

    fun getComments() : List<Comment>?  = commentsLiveData.value?.toOption()?.orNull()?.second
    fun getUser() : User?               = commentsLiveData.value?.toOption()?.orNull()?.first
    fun getPostId() : Long?             = getComments()?.first()?.postId
}