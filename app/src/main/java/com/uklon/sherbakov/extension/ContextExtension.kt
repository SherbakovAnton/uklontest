package com.uklon.sherbakov.extension

import android.content.Context
import android.support.v4.app.Fragment
import android.widget.Toast
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by antony on 08.05.18.
 */


infix fun Context.toast (res : Int) {
    Toast.makeText(this, res, Toast.LENGTH_SHORT).show()
}

infix fun Fragment.toast (res : Int) {
    Toast.makeText(this.activity, res, Toast.LENGTH_SHORT).show()
}

infix fun Fragment.toast (message : String?) {
    Toast.makeText(this.activity, message, Toast.LENGTH_SHORT).show()
}

fun Context.toast (message : String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.handleError(throwable: Throwable) {
    when (throwable.cause) {
        is UnknownHostException,
        is SocketTimeoutException -> {}
    }
}

infix fun Context.string(res: Int) : String = getString(res)
