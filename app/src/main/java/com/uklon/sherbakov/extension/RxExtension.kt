package com.uklon.sherbakov.extension

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by antony on 10.05.18.
 */




fun <T> Single<T>.webCall() : Single<T> = this
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())