package com.uklon.sherbakov.extension

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by antony on 03.04.18.
 */

fun ViewGroup.initialize(res : Int) {
    (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).also { it.inflate(res, this)}
}