package com.uklon.sherbakov.extension

/**
 * Created by antony on 05.04.18.
 */

operator fun Any?.plus(f: Unit?) { f }

operator fun Any?.plus(f: Any?) { f }

operator fun (() -> Unit).plus(f: () -> Unit) { this() + f() }

operator fun <T : Any> (() -> T).plus(f: (T) -> Unit) { this() + f(this()) }

operator fun <T : Any> T.plus(f: (T) -> Any) : T {  f(this) ; return this}

infix  fun <T : Any> T.to(f: (T) -> Unit) {  f(this) }

infix inline fun <P1, R> P1?.pipe(t: (P1) -> R) : R? = this?.let { t(it) }