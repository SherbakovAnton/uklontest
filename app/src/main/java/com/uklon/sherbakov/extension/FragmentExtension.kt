package com.uklon.sherbakov.extension

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.support.v4.app.Fragment
import com.uklon.sherbakov.R

/**
 * Created by antony on 09.05.18.
 */

infix fun <T>  Pair<Fragment, LiveData<T>>.observe(callback: () -> Unit) {
    this.second.observe(this.first, Observer { it?.let {  callback.invoke() } })
}

infix fun <T>  Pair<Fragment, LiveData<T>>.observe(callback: (T) -> Unit) {
    this.second.observe(this.first, Observer { it?.let { it1 -> callback.invoke(it1) } })
}

fun <T> Fragment.observe(liveData: LiveData<T>, callback: (T?) -> Unit) {
    liveData.observe(this, Observer { callback.invoke(it) })
}

fun Fragment.addFragment(root : Int, fragment: Fragment) {
    activity!!
            .supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(root, fragment)
            .addToBackStack(null)
            .commit()
}