package com.uklon.sherbakov.extension

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.View

/**
 * Created by antony on 08.05.18.
 */

infix fun <T>  Pair<FragmentActivity, LiveData<T>>.observe(callback: (T?) -> Unit) {
    this.second.observe(this.first, Observer { callback.invoke(it) })
}

fun <T> FragmentActivity.observe(liveData: LiveData<T>, callback: (T?) -> Unit) {
    liveData.observe(this, Observer { callback.invoke(it) })
}


/************************************ FRAGMENT TRANSACTIONS **********************/

fun FragmentActivity.setFragment(root : View, fragment: Fragment) =
        this.setFragment(root.id, fragment)

fun FragmentActivity.setFragment(root : Int, fragment: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .add(root, fragment)
            .commit()
}

fun FragmentActivity.addFragment(root : Int, fragment: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .replace(root, fragment)
            .addToBackStack(null)
            .commit()
}

fun FragmentActivity.replaceFragment(root : Int, fragment: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .replace(root, fragment)
            .commit()
}