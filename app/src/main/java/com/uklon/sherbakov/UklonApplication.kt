package com.uklon.sherbakov

import android.app.Application
import com.uklon.sherbakov.model.koin.uklonModule
import org.androidannotations.annotations.AfterInject
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EApplication
import org.koin.android.ext.android.startKoin

/**
 * Created by antony on 08.05.18.
 */
@EApplication open class UklonApplication : Application() {

    @AfterInject fun onInit() {
        startKoin(this, listOf(uklonModule))
    }

}